#!/bin/bash
# warsaw slackware package (C) Ruben Carlo Benante <rcb@beco.cc> 2022-11-16

# removing conf files
echo "  --> Deleting etc/warsaw.conf"
rm -rf etc/warsaw.conf
echo "  --> Deleting etc/rc.d/rc.warsaw" 
rm -rf etc/rc.d/rc.warsaw

# removing bin folder
echo "  --> Deleting directory usr/local/etc/warsaw/"
rm -rf usr/local/etc/warsaw/

# Pattern to find warsaw in rc.local
PATBEGIN="warsaw-cut-tag-do-not-remove-begin-HASHz80"
PATEND="warsaw-cut-tag-do-not-remove-end-HASHz80"

if ! grep -qs rc.warsaw etc/rc.d/rc.local ; then
    echo "Warsaw package removed"
    echo
    exit 0
fi

# Remove 11 lines from rc.local between tags
NUMLIN=$(sed -n "/$PATBEGIN/,/$PATEND/p" /etc/rc.d/rc.local | wc -l)
if [ $NUMLIN -ne 11 ] ; then
    echo "Please check your /etc/rc.d/rc.local file for a warsaw entry"
    echo "and remove it manually"
    echo
    exit 0
fi

sed -i.orig "/$PATBEGIN/,/$PATEND/d" /etc/rc.d/rc.local
echo "Warsaw package removed"

if [ -f /etc/rc.d/rc.local.orig ] ; then
    echo "Backup /etc/rc.d/rc.local.orig created"
    echo "Check your rc.local to see if it is safe to remove rc.local.orig"
fi

echo
exit 0

