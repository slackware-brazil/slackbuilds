GammaRay is a software introspection tool for Qt applications developed by KDAB.
Leveraging the QObject introspection mechanism it allows you to observe and
manipulate your application at runtime. This works both locally on your
workstation and remotely on an embedded target.

Augmenting your instruction-level debugger, GammaRay allows you to work on a
much higher level, with the same concepts as the frameworks you use. This is
especially useful for the more complex Qt frameworks such as model/view, state
machines or scene graphs.

When trying to analyze bugs in your application, an instruction-level debugger
is usually your first tool of choice, and rightfully so. Debuggers allow you to
follow the program flow and allow you to inspect the state of objects or
variables. When using a debugger with proper Qt integration, such as in
QtCreator, it will also handle Qt’s own data types correctly. However, when
dealing with complex frameworks this level of detail is often too low-level.
Keeping an overview in a large scene graph or following the complex
interactions between models and views can quickly become a cumbersome task
this way.

GammaRay addresses this by providing domain-specific debugging aids on a much
higher level. It provides easy ways of navigating through the complex internal
structures you find in some Qt frameworks, such as the QtQuick scene graphs,
model/view, QTextDocument, state machines and more. Unlike the debugger,
GammaRay understands those internal structures and can present them in a
meaningful way, making it a tool that should not be missing in any Qt
developer’s toolbox.

GammaRay is available for all major platforms and can inspect applications
right from the start or attach to an already running one at runtime. It also
supports remote debugging, which is especially valuable when working on
embedded systems.
